import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

import { PersonneAddComponent } from './personne-add/personne-add.component';
import {CourseAddComponent} from './course-add/course-add.component';
import { EvenementAddComponent } from './evenement-add/evenement-add.component';
import { PersonneEditComponent } from './personne-edit/personne-edit.component';
import { EvenementEditComponent } from './evenement-edit/evenement-edit.component';
import { CourseEditComponent } from './course-edit/course-edit.component';
import { ClassementComponent } from './classement/classement.component';
import { CheckpointComponent } from './checkpoint/checkpoint.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PersonneListComponent } from './personne-list/personne-list.component';
import { EvenementListComponent } from './evenement-list/evenement-list.component';
import { CourseListComponent } from './course-list/course-list.component';
import { DossardAddComponent } from './dossard-add/dossard-add.component';
import { DossardEditComponent } from './dossard-edit/dossard-edit.component';
import { DossardListComponent } from './dossard-list/dossard-list.component';

const appRoutes: Routes = [
  { path: 'dossard/add', component: DossardAddComponent},
  { path: 'personne/add', component: PersonneAddComponent},
  { path: 'personne/edit', component: PersonneEditComponent},
  { path: 'personne/list', component: PersonneListComponent},
  { path: 'course/add', component: CourseAddComponent},
  { path: 'course/edit', component: CourseEditComponent},
  { path: 'course/list', component: CourseListComponent},
  { path: 'evenement/add', component: EvenementAddComponent},
  { path: 'evenement/edit', component: EvenementEditComponent},
  { path: 'evenement/list', component: EvenementListComponent},
  { path: 'classement', component: ClassementComponent},
  { path: 'checkpoint', component: CheckpointComponent},
  
];

@NgModule({
  declarations: [
    AppComponent,
    PersonneAddComponent,
    CourseAddComponent,
    EvenementAddComponent,
    PersonneEditComponent,
    EvenementEditComponent,
    CourseEditComponent,
    ClassementComponent,
    CheckpointComponent,
    NavigationComponent,
    PersonneListComponent,
    EvenementListComponent,
    CourseListComponent,
    DossardAddComponent,
    DossardEditComponent,
    DossardListComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
