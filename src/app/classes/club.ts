import { Personne } from './personne';

export class Club {
  constructor(
    public id: number,
    public nom: string,
    public adresse: string,
    public cp: string,
    public ville: string,
    public tel: string,
    public mail: string,
    public numerofederal: number,
    public personne: Personne[]
  ) {}
}
