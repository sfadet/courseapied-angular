import {Course} from './course';
import {Personne} from './personne';

export class Dossard {
constructor(
  public id: number,
  public numeroDossard: number,
  public checkppoint: any[],
  public course: Course,
  public personne: Personne
) {}
}
