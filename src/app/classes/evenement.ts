export class Evenement{

  constructor(

    public id: number,
    public label: string,
    public dateDebut: any,
    public dateFin: any,
    public lieu: string,
    public nombreDeCourse: number

  ) {}

}
