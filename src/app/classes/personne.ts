import { Club } from './club';
import { Dossard} from './dossard';

export class Personne {

  constructor(
    public id: number,
    public civilite: string,
    public nom: string,
    public prenom: string,
    public mail: string,
    public tel: string,
    public adresse: string,
    public codePostale: string,
    public ville: string,
    public dateNaissance: string,
    public club: Club,
    public license: string,
    public certificatMedical: boolean,
    public dossards: Dossard[]
  ) {}
}
