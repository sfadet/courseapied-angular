import { Component, OnInit } from '@angular/core';
import {Course} from '../classes/course';
import {CourseService} from '../services/course.service';

@Component({
  selector: 'app-course-add',
  templateUrl: './course-add.component.html',
  styleUrls: ['./course-add.component.scss']
})
export class CourseAddComponent implements OnInit {
  tabCourse: any[] = [];
  formAddCourse: any;

  constructor(
    private dServ: CourseService,
  ) { }

  ngOnInit() {
    this.tabCourse = this.dServ.courses;
    this.formAddCourse = this.resetCourse();
  }

  onAddCourse() {
    this.dServ.addCourse(this.formAddCourse);
  }

  resetCourse() {
    return new Course(null, null, null, null, null, null, null, null);
  }
}
