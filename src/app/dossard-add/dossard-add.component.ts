import { Component, OnInit } from '@angular/core';
import {Dossard} from '../classes/dossard';
import {DossardService} from '../services/dossard.service';

@Component({
  selector: 'app-dossard-add',
  templateUrl: './dossard-add.component.html',
  styleUrls: ['./dossard-add.component.scss']
})
export class DossardAddComponent implements OnInit {

  tabDossard: any[] = [];
  // variable qui récupère les données du formulaire
  formAddDossard: Dossard;

  constructor(
    private dServ: DossardService,
  ) { }

  ngOnInit() {
    this.tabDossard = this.dServ.dossards;
    this.formAddDossard = this.resetDossard();
  }
  onAddDossard() {
    this.dServ.addDossard(this.formAddDossard);
  }

  resetDossard() {
    return new Dossard(null, null, [], null, null);
  }

}
