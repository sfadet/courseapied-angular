import { Component, OnInit } from '@angular/core';
import { Evenement } from '../classes/evenement';
import { EvenementService } from '../services/evenement.service';
import { DateService } from '../services/date.service';

@Component({
  selector: 'app-evenement-add',
  templateUrl: './evenement-add.component.html',
  styleUrls: ['./evenement-add.component.scss']
})
export class EvenementAddComponent implements OnInit {
  tabEvenement: Evenement[];
  formAddEvenement: Evenement;

    // Gestion des tableaux jours/mois/années
    tabjours: string[];
    tabmois: string[];
    tabannees: string[];
    // Récupération du jour/mois/année de naissance
    jourNaiss = '01';
    moisNaiss = '01';
    anneeNaiss = '2012';

  constructor(
    private eServ: EvenementService,
    private dateServ: DateService
  ) { }

  ngOnInit() {
    this.tabEvenement = this.eServ.evenements;
    this.formAddEvenement = this.resetEvenement();
    this.tabjours = this.dateServ.tabJours;
    this.tabmois = this.dateServ.tabMois;
    this.tabannees = this.dateServ.tabAnnees;
  }

  onAddEvenement() {
    this.formAddEvenement.dateDebut =   this.anneeNaiss + '-' + this.moisNaiss + '-' + this.jourNaiss  ;
    this.eServ.addEvenement(this.formAddEvenement);
  }
  resetEvenement() {
    return new Evenement(null, null, null, null, null, null);
  }
}
