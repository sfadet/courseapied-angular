import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EvenementService } from '../services/evenement.service';
import { Evenement } from '../classes/evenement';

@Component({
  selector: 'app-evenement-list',
  templateUrl: './evenement-list.component.html',
  styleUrls: ['./evenement-list.component.scss']
})
export class EvenementListComponent implements OnInit {
  evenements: Evenement[] = [];
  EvenementSubscription: Subscription;

  constructor(private eServ: EvenementService) { }

  ngOnInit() {
    this.EvenementSubscription = this.eServ.evenementSubject.subscribe(
      (evenements: Evenement[]) => {
        this.evenements = evenements;
      }
    );
    this.eServ.getEvenements();
  }

  onSelectDeleteEvenement(label: any, id: any) {
  }
}
