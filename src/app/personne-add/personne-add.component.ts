import { Personne } from './../classes/personne';
import { PersonneService } from './../services/personne.service';
import { Component, OnInit } from '@angular/core';
import { DateService } from '../services/date.service';

@Component({
  selector: 'app-personne-add',
  templateUrl: './personne-add.component.html',
  styleUrls: ['./personne-add.component.scss']
})
export class PersonneAddComponent implements OnInit {

  tabPersonne: any[] = [];
  // variable qui récupère les données du formulaire
  formAddPersonne: Personne;
  // Gestion des tableaux jours/mois/années
  tabjours: string[];
  tabmois: string[];
  tabannees: string[];
  // Récupération du jour/mois/année de naissance
  jourNaiss = '01';
  moisNaiss = '01';
  anneeNaiss = '2012';

  constructor(
    private pServ: PersonneService,
    private dateServ: DateService) { }

  ngOnInit() {
    this.tabPersonne = this.pServ.personnes;
    this.formAddPersonne = this.resetPersonne();
    this.tabjours = this.dateServ.tabJours;
    this.tabmois = this.dateServ.tabMois;
    this.tabannees = this.dateServ.tabAnnees;
  }

  onAddPersonne() {
    this.formAddPersonne.dateNaissance =   this.anneeNaiss + '-' + this.moisNaiss + '-' + this.jourNaiss  ;
    this.pServ.addPersonne(this.formAddPersonne);
  }

  resetPersonne() {
    return new Personne(null, 'mr', '', '', '', '', '', '', '', '', null, '', false, []);
  }

}
