import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personne-edit',
  templateUrl: './personne-edit.component.html',
  styleUrls: ['./personne-edit.component.scss']
})
export class PersonneEditComponent implements OnInit {
  jourNaiss: any;
  tabjours: any;
  moisNaiss: any;
  tabmois: any;
  anneeNaiss: any;
  tabannees: any;
  formEditPersonne: any;

  constructor() { }

  ngOnInit() {
  }

  searchFilter() {
  }

  onSelectDeletePersonne(s: string, id: any) {
  }

  onDeletePersonne(personneDeleteId: any) {
  }

  onUdaptePersonne() {
  }
}
