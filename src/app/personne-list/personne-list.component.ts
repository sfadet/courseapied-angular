import { Component, OnInit } from '@angular/core';
import {Personne} from '../classes/personne';
import {Subscription} from 'rxjs';
import {PersonneService} from '../services/personne.service';


@Component({
  selector: 'app-personne-list',
  templateUrl: './personne-list.component.html',
  styleUrls: ['./personne-list.component.scss']
})
export class PersonneListComponent implements OnInit {

  personnes: Personne[] = [];
  personneSubscription: Subscription;

  constructor(private pServ: PersonneService) { }

  ngOnInit() {
    this.personneSubscription = this.pServ.personneSubject.subscribe(
      (personnes: Personne[]) => {
        this.personnes = personnes;
      }
    );
    this.pServ.getPersonnes();
  }

  onSelectDeletePersonne(s: string, id: number) {
  }
}
