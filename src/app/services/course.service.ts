import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Course} from '../classes/course';


@Injectable({
  providedIn: 'root'
})

export class CourseService {
  courses: Course[] = [];
  courseSubject = new Subject<Course[]>();
  coursedEdit: Course;
  coursedEditSubject = new Subject<Course>();
  private baseUrl = 'http://localhost:8080/courseapied/course';

  constructor(private httpClient: HttpClient) {
    this.getCourse();
  }

  emitCourse() {
    this.courseSubject.next(this.courses);
  }
  emitCoursedEdit() {
    this.coursedEditSubject.next(this.coursedEdit);
  }

  // Récupérer les courses
  getCourse() {
    this.httpClient
      .get<Course[]>(this.baseUrl)
      .subscribe(
        (response) => {
          this.courses = response;
          this.emitCourse();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  // Fonctions CRUD
  ///////////////////////////////////////////////////////////////
  // Ajout d'une course///////////////////////////////////////
  addCourse(course: Course) {
    this.httpClient
      .post(this.baseUrl, course)
      .subscribe(
        () => {
          this.emitCourse();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
}

