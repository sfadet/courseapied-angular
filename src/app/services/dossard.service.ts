import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Dossard} from '../classes/dossard';

@Injectable({
  providedIn: 'root'
})

export class DossardService {
  // La liste des dossard
  dossards: Dossard[] = [];
  dossardSubject = new Subject<Dossard[]>();
  // La personne éditée
  dossardEdit: Dossard;
  dossardEditSubject = new Subject<Dossard>();
  // l'url de base
  private baseUrl = 'http://localhost:8080/courseapied/dossard';

  constructor(private httpClient: HttpClient) {
    this.getDossard();
  }

  emitDossard() {
    this.dossardSubject.next(this.dossards);
  }
  emitDossardEdit() {
    this.dossardEditSubject.next(this.dossardEdit);
  }

  // Récupérer les dossards
  getDossard() {
    this.httpClient
      .get<Dossard[]>(this.baseUrl)
      .subscribe(
        (response) => {
          this.dossards = response;
          this.emitDossard();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  // Fonctions CRUD
  ///////////////////////////////////////////////////////////////
  // Ajout d'une dossard ///////////////////////////////////////
  addDossard(dossard: Dossard) {
    // TODO à tester
    this.httpClient
      .post(this.baseUrl, dossard)
      .subscribe(
        () => {
          this.emitDossard();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
}

