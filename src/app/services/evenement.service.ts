import { Injectable } from '@angular/core';
import { Evenement } from '../classes/evenement';
import {HttpClient} from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EvenementService {

  // La liste des évènements
  evenements: Evenement[] = [];
  evenementSubject = new Subject<Evenement[]>();
  // L'évènement edité
  evenementEdit: Evenement;
  evenementEditSubject = new Subject<Evenement>();
  // URL
  private baseUrl = 'http://localhost:8080/courseapied/evenement';


  constructor(private httpClient: HttpClient) {
    this.getEvenements();
   }

   emitEvenements() {
    this.evenementSubject.next(this.evenements);
  }
  emitEvenementEdit() {
    this.evenementEditSubject.next(this.evenementEdit);
  }

  // Récupérer les évènements
  getEvenements() {
    this.httpClient
      .get<Evenement[]>(this.baseUrl)
      .subscribe(
        (response) => {
          this.evenements = response;
          this.emitEvenements();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  // récupération d'un evenement par son id
  getEvenementById(id: number) {
    this.httpClient
      .get<Evenement>(this.baseUrl + '/' + id)
      .subscribe(
        (response) => {
          this.evenementEdit = response;
          this.emitEvenementEdit();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  // Post : Ajout d'un évenement
  addEvenement(evenement: Evenement) {
    this.httpClient
      .post<Evenement>(this.baseUrl, evenement)
      .subscribe(
        () => {
          this.emitEvenements();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  // Post : Update evenement
  updateEvenement(evenement: Evenement ) {
    this.httpClient
      .put(this.baseUrl, evenement)
      .subscribe(
        () => {
          this.emitEvenements();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  // Post : Delete evenement
  deleteEvenement(id: number) {
    this.httpClient
      .delete(this.baseUrl + '/' + id)
      .subscribe(
        () => {
          this.getEvenements();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }
}
